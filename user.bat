@ECHO OFF
REM Atlassian Thread Dumps for Windows
REM Author: Dave Norton
REM URL: https://bitbucket.org/dave_norton/windows-thread-dumps

ECHO This file will generate thread dumps from the specified process, and prompt for a username and password.
ECHO If your service runs as a system user, run system.bat

CALL setvars.bat

SET /p PID="What is the Process ID? "
SET /p COUNT="How many thread dumps should be generated? "
SET /p INTERVAL="How long should the pause between each dump be in seconds? "
SET /p USERNAME="What is the username the service runs as? "
SET /p PASSWORD="What is the password for the service? "

REM Generate those thread dumps!

ECHO Generating %COUNT% thread dumps every %INTERVAL% seconds for PID %PID%...

FOR /L %%G IN (1, 1, %COUNT%) DO (
	ECHO Executing Thread Dump %%G
	%PSEXEC% -accepteula -s -u %USERNAME% -p %PASSWORD% %JSTACK% -l %PID% >> thread_dump_%DATE:~10,4%%DATE:~4,2%%DATE:~7,2%_%%G.txt
	ECHO Waiting for %INTERVAL% seconds...
	TIMEOUT /T %INTERVAL%
)

ECHO Thread dumps generated successfully!

PAUSE