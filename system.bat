@ECHO OFF
REM Atlassian Thread Dumps for Windows
REM Author: Dave Norton
REM URL: https://bitbucket.org/dave_norton/windows-thread-dumps

ECHO This file will generate thread dumps from the specified process, assuming the service is running in the System account
ECHO If your service runs as a dedicated user, run user.bat

CALL setvars.bat

SET /p PID="What is the Process ID? "
SET /p COUNT="How many thread dumps should be generated? "
SET /p INTERVAL="How long should the pause between each dump be in seconds? "

REM Generate those thread dumps!

ECHO Generating %COUNT% thread dumps every %INTERVAL% seconds for PID %PID%...

FOR /L %%G IN (1, 1, %COUNT%) DO (
	ECHO .
	ECHO Executing Thread Dump %%G
	%PSEXEC% -accepteula -s %JSTACK% -l %PID% >> thread_dump_%DATE:~10,4%%DATE:~4,2%%DATE:~7,2%_%%G.txt
	TIMEOUT /T %INTERVAL%
)

ECHO Thread dumps generated successfully!

PAUSE